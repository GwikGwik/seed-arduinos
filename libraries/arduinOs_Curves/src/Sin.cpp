#include "Sin.h"


//----------------------------------------------------------------
Sin::Sin(TreeNode* parent,String name,
        float f,float A) : 
        Curve(parent,name){

    this->f=f;
    this->A=A;

}
 
//----------------------------------------------------------------
Sin::~Sin(){/*nothing to destruct*/}

//----------------------------------------------------------------

float Sin::solve(float t){
    this->value = this->A*sin(this->f*t);
    return this->value;
}
//----------------------------------------------------------------
