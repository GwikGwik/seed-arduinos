#include <ArduinOS.h>
#include <Programs.h>
#include <Program.h>

ArduinOS root("root");
Programs term((TreeNode *)&root,"bin");
Program tree((TreeNode *)&term,"tree");
Program help((TreeNode *)&term,"help");

/////////////////////////////////////////////////////////////////




///////////////////////////////////////////////////////
void setup() {
///////////////////////////////////////////////////////
Serial.begin(9600);
    title("setup");
    root.setup(); 
  root.print_messages(1);  
    title("query help"); 
    root.query("bin:help");
    title("query tree"); 
    root.query("bin:tree"); 
    title("query toto");    
    root.query("bin:toto"); 
    title("end");       
  root.print_messages(1); 
}
///////////////////////////////////////////////////////
void loop() {
///////////////////////////////////////////////////////


}
///////////////////////////////////////////////////////
