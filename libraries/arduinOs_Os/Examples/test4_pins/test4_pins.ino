#include <MyLib.h>
#include <Test.h>
#include <Frequency.h>
#include <UsbSerial.h>
#include <Pins.h>

///////////////////////////////////////////////////////

Main main_program;

///////////////////////////////////////////////////////
void setup() {
///////////////////////////////////////////////////////

  main_program.append(new TestFrequency("f"));
  main_program.append(new UsbSerialModule("serial",9600,500));
  
  main_program.append(new DigitalInput("digit_1",42));
  main_program.append(new DigitalOutput("digit_2",43,false));
  main_program.append(new AnalogicInput("analogic_1",A1));
  main_program.append(new AnalogicOutput("analogic_2",A2));

  main_program.setup();
  main_program.debug();

}
///////////////////////////////////////////////////////
void loop() {
///////////////////////////////////////////////////////

  main_program.loop();

}
///////////////////////////////////////////////////////
