#include "transforms.h"

/////////////////////////////////////////////////////////////////
//  Scale
/////////////////////////////////////////////////////////////////

//----------------------------------------------------------------
Scale::Scale(TreeNode* parent,String name,float factor) : Curve(parent,name){

    this->factor=factor;
}
 
//----------------------------------------------------------------
Scale::~Scale(){/*nothing to destruct*/}

//----------------------------------------------------------------

float Scale::solve(float t){

    this->value=this->factor*this->curves[0]->value;
    return this->value;
}
//----------------------------------------------------------------

/////////////////////////////////////////////////////////////////
//  Offset
/////////////////////////////////////////////////////////////////

//----------------------------------------------------------------
Offset::Offset(TreeNode* parent,String name,float offset) : Curve(parent,name){

    this->offset=offset;
}
 
//----------------------------------------------------------------
Offset::~Offset(){/*nothing to destruct*/}

//----------------------------------------------------------------

float Offset::solve(float t){


    this->value=this->offset+this->curves[0]->value;
    return this->value;
}
//----------------------------------------------------------------


/////////////////////////////////////////////////////////////////
//  Range
/////////////////////////////////////////////////////////////////



//----------------------------------------------------------------
Range::Range(TreeNode* parent,String name,float min,float max) : Curve(parent,name){
    this->min=min;
    this->max=max;
}
 
//----------------------------------------------------------------
Range::~Range(){/*nothing to destruct*/}

//----------------------------------------------------------------

float Range::solve(float t){
//https://www.arduino.cc/reference/en/language/functions/math/constrain/
//constrain(x, a, b)
    float v=this->curves[0]->value;

    if (v > this->max) {

        this->value=this->max;

    }else if(v < this->min) {
        this->value=this->min;

    }else{
        this->value=v;
    }
    return this->value;
}
//----------------------------------------------------------------

/////////////////////////////////////////////////////////////////


