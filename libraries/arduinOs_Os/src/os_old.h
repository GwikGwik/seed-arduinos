#include <Root.h>
#include <Command.h>
#include <Action.h>
#include <Command.h>
#include <Module.h>
#include <Programs.h>
#include <Program.h>
#include <UsbSerial.h>
#include <PrintTree.h>

Root root("root");

Module devices(&root,"dev");
Module modules(&root,"mods");
Programs term(&root,"bin");


PrintTree tree(&term,"tree","",10);
PrintTree help(&term,"h","bin",1);
PrintTree dev(&term,"dev","dev",1);
PrintTree mods(&term,"mods","mods",1);
UsbSerial serial(&term,"serial");

Action gps(&devices,"gps");
Action sd(&devices,"sd");

Action env(&modules,"env");
