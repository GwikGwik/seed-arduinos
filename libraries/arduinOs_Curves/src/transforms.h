#ifndef transforms_H
#define transforms_H

#include <Curve.h>
/////////////////////////////////////////////////////////////////
class Random : public Curve {
/////////////////////////////////////////////////////////////////
//only one children
/*
  randomSeed(analogRead(0));

random(max)
random(min, max)
*/
    public:
    

       Abs(TreeNode* parent,String name):Curve(parent,name){};
       ~Abs(){};
        virtual float solve(float t){};
};
/////////////////////////////////////////////////////////////////
class Sqrt : public Curve {
/////////////////////////////////////////////////////////////////
//only one children

    public:
    

       Abs(TreeNode* parent,String name):Curve(parent,name){};
       ~Abs(){};
        virtual float solve(float t){};
};
/////////////////////////////////////////////////////////////////
class Abs : public Curve {
/////////////////////////////////////////////////////////////////
//only one children

    public:
    

       Abs(TreeNode* parent,String name):Curve(parent,name){};
       ~Abs(){};
        virtual float solve(float t){};
};

/////////////////////////////////////////////////////////////////
class Abs : public Curve {
/////////////////////////////////////////////////////////////////
//only one children

    public:
    

       Abs(TreeNode* parent,String name):Curve(parent,name){};
       ~Abs(){};
        virtual float solve(float t){};
};

/////////////////////////////////////////////////////////////////
class Opposite : public Curve {
/////////////////////////////////////////////////////////////////
//only one children

    public:
    

       Opposite(TreeNode* parent,String name):Curve(parent,name){};
       ~Opposite(){};
        virtual float solve(float t){};
};
/////////////////////////////////////////////////////////////////
class Inverse : public Curve {
/////////////////////////////////////////////////////////////////
//only one children

    public:
    

       Inverse(TreeNode* parent,String name):Curve(parent,name){};
       ~Inverse(){};
        virtual float solve(float t){};
};
/////////////////////////////////////////////////////////////////
class Scale : public Curve {
/////////////////////////////////////////////////////////////////
//only one children

    public:
    
        float factor=1.0;

       Scale(TreeNode* parent,String name,float factor);
       ~Scale();
        virtual float solve(float t);
};
/////////////////////////////////////////////////////////////////
class Offset : public Curve {
/////////////////////////////////////////////////////////////////
//only one children

    public:
        float offset=0.0;


       Offset(TreeNode* parent,String name,float offset);
       ~Offset();
        virtual float solve(float t);
};



/////////////////////////////////////////////////////////////////
class Range : public Curve {
/////////////////////////////////////////////////////////////////
//only one children

    public:
        float min=1.0;
        float max=1.0;

       Range(TreeNode* parent,String name,float min,float max);
       ~Range();
        virtual float solve(float t);
};

/////////////////////////////////////////////////////////////////






#endif
