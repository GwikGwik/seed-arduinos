#include "Pins.h"

//////////////////////////////////////////////////////////////////
// DigitalInput
//////////////////////////////////////////////////////////////////

//----------------------------------------------------------------
DigitalInput::DigitalInput(TreeNode* parent,String name,int pin) : SinglePin(parent,name,pin,INPUT){
    this->is_output=false;
}
//----------------------------------------------------------------
void DigitalInput::onRead(void){
     this->state = digitalRead(this->pin); 
     //this->log("read "+String(this->state));
}
//----------------------------------------------------------------

//////////////////////////////////////////////////////////////////
// DigitalOutput
//////////////////////////////////////////////////////////////////

//----------------------------------------------------------------
DigitalOutput::DigitalOutput(TreeNode* parent,String name,int pin,bool state) : SinglePin(parent,name,pin,OUTPUT){
    this->is_input=false;
    if(state){
        this->set_on();
    }else{
        this->set_off();
    }

}
//----------------------------------------------------------------
void DigitalOutput::onWrite(void){
    if(state){
        this->set_on();
    }else{
        this->set_off();
    }
}
//----------------------------------------------------------------
void DigitalOutput::set_on(void){
     this->state=HIGH;
     digitalWrite(this->pin, HIGH); 
}
//----------------------------------------------------------------
void DigitalOutput::set_off(void){
     this->state=LOW;
     digitalWrite(this->pin, LOW); 
}
//----------------------------------------------------------------

//////////////////////////////////////////////////////////////////
// AnalogicInput
//////////////////////////////////////////////////////////////////

//----------------------------------------------------------------
AnalogicInput::AnalogicInput(TreeNode* parent,String name,int pin) : SinglePin(parent,name,pin,INPUT){
    this->is_output=false;

}
//----------------------------------------------------------------
void AnalogicInput::onRead(void){
     this->state = analogRead(this->pin); 
     //this->log("read "+String(this->state));
}
//----------------------------------------------------------------


//////////////////////////////////////////////////////////////////
// AnalogicOutput
//////////////////////////////////////////////////////////////////

//----------------------------------------------------------------
AnalogicOutput::AnalogicOutput(TreeNode* parent,String name,int pin) : SinglePin(parent,name,pin,OUTPUT){
    this->set(0);
    this->is_input=false;

}
//----------------------------------------------------------------
void AnalogicOutput::onWrite(void){
    this->set(0);
    //this->send_message("read",this->state);
}
//----------------------------------------------------------------
void AnalogicOutput::set(int value){
     analogWrite(this->pin, value); 
}
//----------------------------------------------------------------

//////////////////////////////////////////////////////////////////




