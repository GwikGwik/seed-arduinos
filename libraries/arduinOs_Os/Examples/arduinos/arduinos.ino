#include <ArduinOS.h>

ArduinOS os("main");
Action  runner(&os,"act0");
TestAction  t0(&runner,"act0");
TestAction  t1(&runner,"act1");
TestAction  t2(&runner,"act2");

DeviceManager  devices(&os,"act0");
TestDevice a(&devices,"dev0");
TestDevice b(&devices,"dev1");
TestDevice c(&devices,"dev2");

/////////////////////////////////////////////////////////////////




///////////////////////////////////////////////////////
void setup() {
///////////////////////////////////////////////////////
    os.runner=&runner;
    os.devices=&devices;
    Serial.begin(9600);
    title("tree");
    os.tree(); 
    title("setup");
    os.setup();
    title("loop"); 
    os.loop(); 

}
///////////////////////////////////////////////////////
void loop() {
///////////////////////////////////////////////////////


}
///////////////////////////////////////////////////////
