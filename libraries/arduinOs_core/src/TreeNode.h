#ifndef TREENODE_H
#define TREENODE_H

#include <tools.h>
/////////////////////////////////////////////////////////////////
class Messages {
/////////////////////////////////////////////////////////////////
    //arx::vector<String> messages;
        //void log_clear(void);
        //String print_messages(void);
        //void log(String message){ this->root()->messages.push_back(String(message)); Serial.println(message);};
};
/////////////////////////////////////////////////////////////////
class TreeNode {
/////////////////////////////////////////////////////////////////

    /* 
    simple tree structure for arduino
    base for Blackboard_* libraries
    handling a tree structure to control execution
    and communications
    */ 

    public:

    // Tree Structure
    //------------------------------------------------
    String name;                            //name of the node
    String node_id;                         //node_id of the node
    TreeNode *parent=NULL;                  //reference to parent
    arx::vector <TreeNode *> children;      //reference vector to children
    int position;                            //children position

    // activation
    //------------------------------------------------
    bool active=true;                            //
    void on(void){this->active=true;};
    void off(void){this->active=false;};


    // Create/destroy
    //------------------------------------------------
    TreeNode();  
    TreeNode(String name);     
    TreeNode(TreeNode* parent,String name); 
    ~TreeNode();    //destroy

    // life cycle
    //------------------------------------------------

        void setup(void);
        virtual void onSetup(void){};

    // Tree Visualisation
    //------------------------------------------------

        void tree(void);                        // print tree to serial
        void tree(int level);                   // print subtree to serial
        void log(String message);

    // Tree handling
    //------------------------------------------------

        String path(void);                      // return path of the node /a/b/c
        TreeNode* find_child(String name);  
        TreeNode* find(String path);            // return reference to a node from his path
        TreeNode* root(void);                   // return reference to root

    //------------------------------------------------
        template <typename T>
        T* root(void) { 
            return (T*)this->root();
        } 
    //------------------------------------------------
        template <typename T>
        T* find(String path) { 
            return (T*)this->find(path);
        } 
    //------------------------------------------------
        template <typename T>
        arx::vector <T *> getChildren (void) { 

            arx::vector <T *> actions;
            T* node;
            for (unsigned i=0; i < this->children.size(); i++) {
                    
                node = (T *) this->children[i];
                if (node != nullptr and node->active) {
                    actions.push_back(node);
                }
            }
            return actions;
        }
    //------------------------------------------------
        template <typename T>
        arx::vector <T *> getNodes(void){

            arx::vector <T *> nodes=this->getChildren<T>();
            arx::vector <T *> subnodes;

            for (unsigned i=0; i < this->children.size(); i++) {
                if (this->children[i]->active) {
                    subnodes=this->children[i]->getNodes<T>();
                    for (unsigned j=0; j < subnodes.size(); j++) {
                            nodes.push_back(subnodes[j]);
                    }
                }
            }
            return nodes;
        }
    //------------------------------------------------

};
/////////////////////////////////////////////////////////////////
class TestTree : public TreeNode {
/////////////////////////////////////////////////////////////////

    public:                
        TestTree(String name) : TreeNode(name){};
        TestTree(TreeNode* parent,String name):TreeNode(parent,name){}; 
        virtual void onSetup(void){this->log("setup");};
        virtual void test(void){this->log("TestTree");};
};

/////////////////////////////////////////////////////////////////    


#endif


//----------------------------------------------------------------



