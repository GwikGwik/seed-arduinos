#ifndef MODULE_H
#define MODULE_H

#include <TreeNode.h>
#include <Action.h>
#include <Device.h>
/////////////////////////////////////////////////////////////////
class Module : public TreeNode {
/////////////////////////////////////////////////////////////////


    public:
        //arx::vector <Module *> modules;


       Module(TreeNode* parent,String name);
       ~Module();
       virtual void onCall(void){};
        virtual void query(String path,String command);
        virtual void onQuery(TreeNode* obj,String command);

    // Lifecycle
    //------------------------------------------------

        // recursive execution
        //void setup(void);
        void call(JsonObject data){

            //this->read();
            //this->exec();
            //this->write();

        };
        void read(void);
        void exec(void);
        void write(void);
        void debug(void);

        // methods to overload

        virtual void onDebug(void){};



        virtual void onSetup(void){
            //this->devices=this->getNodes<Device>();
            //this->modules=this->getChildren<Module>();
        }; 

        virtual void onRead(void){

            //for (unsigned i=0; i < this->devices.size(); i++) {
            //    this->devices[i]->read();
            //}
        }; 

        virtual void onExec(void){
            arx::vector <Action *> actions=this->getChildren<Action>();
            for (unsigned i=0; i < actions.size(); i++) {
                actions[i]->call();
            }

            //for (unsigned i=0; i < this->modules.size(); i++) {
            //    this->modules[i]->call();
            //}
        };

        virtual void onWrite(void){
            //for (unsigned i=0; i < this->devices.size(); i++) {
            //    this->devices[i]->write();
           // }
        }; 

};
/////////////////////////////////////////////////////////////////


#endif
