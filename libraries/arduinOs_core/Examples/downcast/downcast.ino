
#include <TreeNode.h>
/////////////////////////////////////////////////////////////////
class A : public TestTree {

    public:
        A(String name) : TestTree(name){};
        A(TreeNode* parent,String name) : TestTree(parent,name){};
        ~A(){};
        test(){Serial.println("tes A "+this->node_id);};
};
class B : public TestTree {

    public:
        B(TreeNode* parent,String name) : TestTree(parent,name){};
        ~B(){};
        test(){Serial.println("test B "+this->node_id);};

};
///////////////////////////////////////////////////////
// construire un arbre d'éléments
///////////////////////////////////////////////////////

A root("main");

A a(&root,"a");
B b(&root,"b");

///////////////////////////////////////////////////////
void setup() {
///////////////////////////////////////////////////////
    //initialiser le port série
    Serial.begin(9600);
    //afficher la structure
    root.tree();
    root.setup();

    A* elt=root.getNode<A>("a");
    elt->test();

    B* elt2=root.getNode<B>("b");
    elt2->test();
//erreur
//tous les objets marchent
    B* elt3=root.getNode<B>("a");
    if(elt3){elt3->test();}



    Serial.println("------TestTree---------------");
    std::vector <TestTree*> children1=root.getNodes<TestTree>();
    for (unsigned i=0; i <children1.size(); i++) {
        children1[i]->test();
    }
//marche pas

    Serial.println("------A---------------");
    std::vector <A*> children=root.getNodes<A>();
    for (unsigned i=0; i <children.size(); i++) {
        children[i]->test();
    }

    Serial.println("------B---------------");
    std::vector <TestTree*> children2=root.getNodes<TestTree>();
    for (unsigned i=0; i <children2.size(); i++) {
        children2[i]->test();
    }
}
///////////////////////////////////////////////////////
void loop() {
///////////////////////////////////////////////////////

  //vide

}
///////////////////////////////////////////////////////
