#ifndef ACTION_H
#define ACTION_H

#include <TreeNode.h>

/////////////////////////////////////////////////////////////////
class Action : public TreeNode {
/////////////////////////////////////////////////////////////////

    /* 
    simple tree structure for arduino
    base for programs
    */ 

    public:



    // Create/destroy
    //------------------------------------------------
        Action():TreeNode(){};   
        Action(String name):TreeNode(name){};                  
        Action(TreeNode* parent,String name):TreeNode(parent,name){}; 
        ~Action(){};                          //destroy, not done

    // Lifecycle
    //------------------------------------------------

        virtual void call(void);
        virtual void onCall(void){this->callChildren();};
        virtual void callChildren(void); // children execution by default

    // Tools
    //------------------------------------------------

        float now(void);  

    //------------------------------------------------
    private:
};
/////////////////////////////////////////////////////////////////
class TestAction : public Action {
/////////////////////////////////////////////////////////////////

    public:      
        TestAction(String name):Action(name){};           
        TestAction(TreeNode* parent,String name):Action(parent,name){}; 
        virtual void onSetup(void){this->log("setup");};
        virtual void onCall(void){this->log("call");};

};

/////////////////////////////////////////////////////////////////
#endif

