
#include <TreeNode.h>

/////////////////////////////////////////////////////////////////
class TestNode : public TreeNode {
/////////////////////////////////////////////////////////////////

    public:
        TestNode(TreeNode* parent,String name) : TreeNode(parent,name){

        };

        TestNode(String name) : TreeNode(name){

        };
        
        ~TestNode(){

        };

        void onSetup(void){Serial.println("onSetup"+this->path());};
        void onDebug(void){Serial.println("onDebug"+this->path());};
        void onRead(void){Serial.println("onRead"+this->path());};
        void onExec(void){Serial.println("onExec"+this->path());};
        void onWrite(void){Serial.println("onWrite"+this->path());};


};

/////////////////////////////////////////////////////////////////


TestNode root("main");

TestNode a(&root,"a");
TestNode b(&root,"b");
TestNode c(&root,"c");

TestNode aa(&a,"a");
TestNode ab(&a,"b");
TestNode ac(&a,"c");


TestNode ba(&b,"a");
TestNode bb(&b,"b");
TestNode bc(&b,"c");

///////////////////////////////////////////////////////
void setup() {
///////////////////////////////////////////////////////
  Serial.begin(9600);


  root.tree();
  root.setup();
  //root.debug();
  //root.loop();


}
///////////////////////////////////////////////////////
void loop() {
///////////////////////////////////////////////////////


}
///////////////////////////////////////////////////////
