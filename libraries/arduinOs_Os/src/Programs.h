#ifndef BIN_H
#define BIN_H

#include <Module.h>
#include <Program.h>
/////////////////////////////////////////////////////////////////
class Programs : public Module {
/////////////////////////////////////////////////////////////////


    public:


       Programs(TreeNode* parent,String name);
       ~Programs();

        virtual void onSetup(void);
        virtual void onQuery(TreeNode* obj,String command);

};
/////////////////////////////////////////////////////////////////


#endif
