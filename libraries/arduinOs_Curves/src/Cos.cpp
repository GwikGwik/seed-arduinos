#include "Cos.h"


//----------------------------------------------------------------
Cos::Cos(TreeNode* parent,String name,float f,float A) : Curve(parent,name){
    this->f=f;
    this->A=A;
}
 
//----------------------------------------------------------------
Cos::~Cos(){/*nothing to destruct*/}

//----------------------------------------------------------------

float Cos::solve(float t){
    this->value= this->A*cos(this->f*t);
    return this->value;
}
//----------------------------------------------------------------
